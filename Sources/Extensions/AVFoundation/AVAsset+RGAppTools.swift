//
//  AVAsset+RGAppTools.swift
//  RGAppTools
//
//  Created by RAIN on 2023/7/25.
//  Copyright © 2023 Smartech. All rights reserved.
//

import AVFoundation
import UIKit

extension RGAppTools where Base: AVAsset {

  /// 视频编码类型是否为 H.264
  @available(iOS 13.0, *)
  public var isH264: Bool {
    var isH264 = false
    let videoTrack = videoAssetTrack
    if let videoTrack = videoTrack {
      isH264 = videoTrack.rat.isH264
    }
    return isH264
  }

  /// 视频编码类型是否为 HEVC
  @available(iOS 13.0, *)
  public var isHEVC: Bool {
    var isHEVC = false
    let videoTrack = videoAssetTrack
    if let videoTrack = videoTrack {
      isHEVC = videoTrack.rat.isHEVC
    }
    return isHEVC
  }

  /// 从 AVAsset 的轨道中取出视频轨道
  public var videoAssetTrack: AVAssetTrack? {
    let trackArray = base.tracks
    var videoTrack: AVAssetTrack?
    for track in trackArray where track.mediaType == .video {
      videoTrack = track
    }
    return videoTrack
  }

  /// 将 `HEVC` 编码的视频转换为 `H.264` 编码视频
  /// - Parameters:
  ///   - inputURL: 输入视频的 URL
  ///   - outputURL: 输出视频的 URL
  ///   - completion: 转码后的操作
  public static func convertHEVCtoH264(
    inputURL: URL,
    outputURL: URL,
    completion: @escaping (Error?) -> Void
  ) {
    let asset = AVAsset(url: inputURL)

    guard let exportSession = AVAssetExportSession(
      asset: asset,
      presetName: AVAssetExportPresetMediumQuality
    ) else {
      completion(NSError(
        domain: "RGAppTools.AVAsset.error",
        code: 0,
        userInfo: [NSLocalizedDescriptionKey: "Failed to create export session"]
      ))
      return
    }

    exportSession.outputURL = outputURL
    exportSession.outputFileType = .mp4
    exportSession.shouldOptimizeForNetworkUse = true

    exportSession.exportAsynchronously {
      switch exportSession.status {
      case .completed:
        completion(nil)

      case .failed:
        completion(exportSession.error)

      case .cancelled:
        completion(NSError(
          domain: "RGAppTools.AVAsset.error",
          code: 0,
          userInfo: [NSLocalizedDescriptionKey: "Export cancelled"]
        ))

      default:
        completion(NSError(
          domain: "RGAppTools.AVAsset.error",
          code: 0,
          userInfo: [NSLocalizedDescriptionKey: "Export failed with unknown status"]
        ))
      }
    }
  }

  /// 获取视频首帧
  /// - Parameter fileURL: 视频路径 URL
  /// - Returns: 视频首帧图片
  public static func firstFrame(of fileURL: URL) -> UIImage? {
    let asset = AVAsset(url: fileURL)
    let assetGen = AVAssetImageGenerator(asset: asset)
    assetGen.appliesPreferredTrackTransform = true
    let time = CMTimeMake(value: 0, timescale: 600)
    var actualTime: CMTime = .zero
    do {
      let cgImage = try assetGen.copyCGImage(at: time, actualTime: &actualTime)
      let image = UIImage(cgImage: cgImage)
      return image
    } catch {
#if DEBUG
      print("RGAppTools.AVAsset.error: \n\(error)")
#endif
      return nil
    }
  }

}

//
//  AVAssetTrack+RGAppTools.swift
//  RGAppTools
//
//  Created by RAIN on 2023/7/25.
//  Copyright © 2023 Smartech. All rights reserved.
//

import AVFoundation

extension RGAppTools where Base: AVAssetTrack {

  /// 视频编码类型是否为 H.264
  @available(iOS 13.0, *)
  public var isH264: Bool {
    var isH264 = false
    let type = mediaSubType
    isH264 = mediaSubType == .h264
    return isH264
  }

  /// 视频编码类型是否为 HEVC
  @available(iOS 13.0, *)
  public var isHEVC: Bool {
    var isHEVC = false
    let type = mediaSubType
    isHEVC = mediaSubType == .hevc
    return isHEVC
  }

  /// 从视频格式信息中获取视频编码类型
  @available(iOS 13.0, *)
  public var mediaSubType: CMFormatDescription.MediaSubType {
    let formatDescription = cmFormatDescription
    let mediaSubType = formatDescription.mediaSubType
    return mediaSubType
  }

  /// 从视频轨道中获取视频格式信息
  public var cmFormatDescription: CMFormatDescription {
    let formatDescription = base.formatDescriptions.first
    return formatDescription as! CMFormatDescription
  }

}
